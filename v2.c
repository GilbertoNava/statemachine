#include <stdio.h>

typedef enum
{
	goNORTE,
	waitNORTE,
	goESTE,
	waitESTE
} States;

typedef enum
{
	NO_CARROS,
	CARROS_ESTE,
	CARROS_NORTE,
	AMBOS_LADOS
} Inputs;

void PrintSM(int State)
{
	switch (State)
	{
	case 0:
		printf("goNORTE");
		break;
	case 1:
		printf("waitNORTE");
		break;
	case 2:
		printf("goESTE");
		break;
	case 3:
		printf("waitESTE");
		break;
	}
};

int E_Actual;
void SelectSM(int Salida, int Entrada)
{
	States Siguiente;
	switch (Entrada)
	{
	case 0:
		switch (Salida)
		{
		case 0:
			Siguiente = goNORTE;
			break;
		case 1:
			Siguiente = waitNORTE;
			break;
		case 2:
			Siguiente = goNORTE;
			break;
		case 3:
			Siguiente = waitNORTE;
			break;
		}
		break;
	case 1:
		Siguiente = goESTE;
		break;
	case 2:
		switch (Salida)
		{
		case 0:
			Siguiente = goESTE;
			break;
		case 1:
			Siguiente = goESTE;
			break;
		case 2:
			Siguiente = waitESTE;
			break;
		case 3:
			Siguiente = waitESTE;
			break;
		}
		break;
	case 3:
		Siguiente = goNORTE;
		break;
	}
	E_Actual = Siguiente;
};

int main()
{
	//Secuencia de demostracion
	int Machine[4] = {NO_CARROS, CARROS_ESTE, CARROS_NORTE, AMBOS_LADOS};
	States Siguiente;
	Siguiente = 0;
	E_Actual = Siguiente;//goNORTE
	int i;
	while(i <= 5)
	{
		printf("\n En el estado [");
		PrintSM(E_Actual);
		printf("]");
		SelectSM(Machine[i], E_Actual);
		i++;
	}
	return 0;
}