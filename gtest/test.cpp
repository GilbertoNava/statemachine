#include "SM.c"
#include <gtest/gtest.h>

TEST(State0, Machine0){
    ASSERT_EQ(goNORTE, SelectSM(NO_CARROS,goNORTE));
}
TEST(State1, Machine1){
    ASSERT_EQ(goNORTE, SelectSM(CARROS_NORTE,goNORTE));
}
TEST(State2, Machine2){
    ASSERT_EQ(waitNORTE, SelectSM(CARROS_ESTE,goNORTE));
}
TEST(State3, Machine3){
    ASSERT_EQ(waitNORTE, SelectSM(AMBOS_LADOS,goNORTE));
}



TEST(State4, Machine4){
    ASSERT_EQ(goESTE, SelectSM(NO_CARROS,waitNORTE));
}
TEST(State5, Machine5){
    ASSERT_EQ(goESTE, SelectSM(CARROS_ESTE,waitNORTE));
}
TEST(State6, Machine6){
    ASSERT_EQ(goESTE, SelectSM(CARROS_NORTE,waitNORTE));
}
TEST(State7, Machine7){
    ASSERT_EQ(goESTE, SelectSM(AMBOS_LADOS,waitNORTE));
}



TEST(State8, Machine8){
        ASSERT_EQ(goESTE, SelectSM(NO_CARROS,goESTE));
}
TEST(State9, Machine9){
        ASSERT_EQ(goESTE, SelectSM(CARROS_ESTE,goESTE));    
}
TEST(State10, Machine10){
        ASSERT_EQ(waitESTE, SelectSM(CARROS_NORTE,goESTE));
}
TEST(State11, Machine11){
        ASSERT_EQ(waitESTE, SelectSM(AMBOS_LADOS,goESTE));
}



TEST(State12, Machine12){
        ASSERT_EQ(goNORTE, SelectSM(NO_CARROS,waitESTE));
}
TEST(State13, Machine13){
        ASSERT_EQ(goNORTE, SelectSM(CARROS_ESTE,waitESTE));
}
TEST(State14, Machine14){
        ASSERT_EQ(goNORTE, SelectSM(CARROS_NORTE,waitESTE));
}
TEST(State15, Machine15){
        ASSERT_EQ(goNORTE, SelectSM(AMBOS_LADOS,waitESTE));
}


int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
