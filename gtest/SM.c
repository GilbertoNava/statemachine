#include <stdio.h>

typedef enum
{
	goNORTE,
	waitNORTE,
	goESTE,
	waitESTE
} States;

typedef enum
{
	NO_CARROS,
	CARROS_ESTE,
	CARROS_NORTE,
	AMBOS_LADOS
} Inputs;

void PrintSM(int State)
{
	switch (State)
	{
	case 0:
		printf("goNORTE");
		break;
	case 1:
		printf("waitNORTE");
		break;
	case 2:
		printf("goESTE");
		break;
	case 3:
		printf("waitESTE");
		break;
	}
};

int SelectSM(int Salida, int Entrada)
{
	States Siguiente;
	switch (Entrada)
	{
	case 0:
		switch (Salida)
		{
		case 0:
			Siguiente = goNORTE;
			break;
		case 1:
			Siguiente = waitNORTE;
			break;
		case 2:
			Siguiente = goNORTE;
			break;
		case 3:
			Siguiente = waitNORTE;
			break;
		}
		break;
	case 1:
		Siguiente = goESTE;
		break;
	case 2:
		switch (Salida)
		{
		case 0:
			Siguiente = goESTE;
			break;
		case 1:
			Siguiente = goESTE;
			break;
		case 2:
			Siguiente = waitESTE;
			break;
		case 3:
			Siguiente = waitESTE;
			break;
		}
		break;
	case 3:
		Siguiente = goNORTE;
		break;
	}
	return Siguiente;	
};
